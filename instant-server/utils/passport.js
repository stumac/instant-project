const passport = require('passport');
const User = require('../data_store/User');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const LocalStrategy = require('passport-local');

class Passport {
    constructor() {
        const localOptions = { usernameField: 'user-email', passwordField: 'user-password' };

        const jwtOptions = {
            // Telling Passport to check authorization headers for JWT
            jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('jwt'),
            // Telling Passport where to find the secret
            secretOrKey: 'thisIsASuperSecretKeyAndThisSHouldNotBeHardcoded',
        };
        this.localLogin = new LocalStrategy(localOptions, (email, password, done) => {
            console.log('am I even being hit????');
            User.findOne({ email }, (err, user) => {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    return done(null, false, { error: 'User not found' });
                }

                user.comparePassword(password, (passwordErr, matched) => {
                    if (passwordErr) {
                        return done(err);
                    }
                    if (!matched) {
                        return done(null, false, { error: 'Incorrect Password' });
                    }
                    return done(null, user);
                });
            });
        });

        this.jwtLogin = new JwtStrategy(jwtOptions, ((payload, done) => {
            User.findById(payload._id, (err, user) => {
                if (err) { return done(err, false); }
                if (user) {
                    done(null, user);
                } else {
                    done(null, false);
                }
            });
        }));

        passport.use(this.jwtLogin);
        passport.use(this.localLogin);
    }
}

module.exports = new Passport();
