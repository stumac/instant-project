const express = require('express');
const passport = require('passport');
const RecipeInteractor = require('../lib/RecipeInteractor');

const requireAuth = passport.authenticate('jwt', { session: false });

const router = express.Router();
router.use(requireAuth);
/* GET users listing. */
router.get('/', (req, res) => {
    RecipeInteractor.getRecipes().then(result =>
        res.json(result)).catch(error => res.status(500).json(error));
});
router.get('/:id', (req, res) => {

});
router.get('/myRecipes', (req, res) => {

});
module.exports = router;
