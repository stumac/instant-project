const express = require('express');

const router = express.Router();
const AuthenticationController = require('../controllers/AuthenticationController');
const passportService = require('../utils/passport');
const passport = require('passport');
const parser = require('body-parser');

const urlencodedParser = parser.urlencoded({ extended: false });
// const requireAuth = passport.authenticate('jwt', { session: false });
const requireLogin = passport.authenticate('local', { session: false });

router.post('/', urlencodedParser, requireLogin, AuthenticationController.login);

router.post('/refreshToken', AuthenticationController.refreshToken);
module.exports = router;
