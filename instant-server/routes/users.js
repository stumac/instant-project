const express = require('express');
const passport = require('passport');

const requireAuth = passport.authenticate('jwt', { session: false });

const router = express.Router();
/* GET users listing. */
router.get('/', requireAuth, (req, res) => res.send('respond with a resource'));

module.exports = router;
