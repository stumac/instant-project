const Recipe = require('../data_store/Recipe');

class RecipeInteractor {
    static getRecipes() {
        return new Promise((resolve, reject) => {
            const query = Recipe.find();
            query.exec((err, res) => {
                if (err) {
                    return reject(err);
                }
                console.log(res);
                return resolve(res);
            });
        });
    }
}

module.exports = RecipeInteractor;
