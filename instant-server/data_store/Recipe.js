const mongoose = require('mongoose');

const RecipeSchema = new mongoose.Schema({
    title: String,
    author: String, // could make this an objectID reference
    difficulty: String, // could easily be an enum
    introduction: String,
    category: String, // could be a reference to another collection
    duration: String, // String for now, but could easily be an enum
    // this could probably be a reference to an ingredients collection for interesting metadata
    ingredients: String,
    instructions: String,
    date: { type: Date, default: Date.now },
    hidden: Boolean,
});

module.exports = mongoose.model('Recipe', RecipeSchema);
