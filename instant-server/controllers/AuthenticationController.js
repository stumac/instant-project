const jwt = require('jsonwebtoken');
// const crypto = require('crypto');
// const User = require('../data_store/User');
// config = require('../config/main');
const refreshToken = 'thisIsATokenThatShouldBeGeneratedAndLongLivingButForNowAStringWillDo';
const User = require('../data_store/User');

class AuthenticationController {
    static generateToken(user) {
        return jwt.sign(user, 'thisIsASuperSecretKeyAndThisSHouldNotBeHardcoded', {
            expiresIn: 10080, // in seconds
        });
    }
    static generateRefreshToken(user) {
        return jwt.sign(user, 'thisIsASuperSecretKeyAndThisSHouldNotBeHardcoded', {
            expiresIn: 864000 * 3, // 10 days in seconds times 3 to create a 30 day refresh token
        });
    }
    static setRefreshTokenInfo(request) {
        return {
            _id: request._id,
            name: request.name,
            email: request.email,
            refresh_token: AuthenticationController.generateRefreshToken(request),
        };
    }
    static setUserInfo(request) {
        return {
            _id: request._id,
            name: request.name,
            email: request.email,
            // refreshToken: AuthenticationController.setRefreshTokenInfo({
            //     _id: request._id,
            //     name: request.name,
            //     email: request.email,
            // }),
        };
    }

    static login(req, res) {
        const userInfo = AuthenticationController.setUserInfo(req.user);

        return res.status(200).json({
            token: `JWT ${AuthenticationController.generateToken(userInfo)}`,
            user: userInfo,
        });
    }

    static refreshToken(req, res) {
        let header = req.get('Authorization');
        header = header.replace('JWT ', '');
        const token = jwt.decode(header);
        if (token.refreshToken === refreshToken) {
            const userInfo = AuthenticationController.setUserInfo(req.user);
            return res.status(200).json({
                token: `JWT ${AuthenticationController.generateToken(userInfo)}`,
                user: userInfo,
            });
        }
        return res.status(401).json({ error: 'refresh token does not exist or is not valid' });
    }
}

module.exports = AuthenticationController;
