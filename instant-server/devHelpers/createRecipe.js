// just a small script to create a recipe

const Recipe = require('../data_store/Recipe');
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017');
let recipe = new Recipe({
    title: 'Spinach Chicken Alfredo',
    author: '5a10c5846b75b17dd30ab90e',
    difficulty: 'Medium',
    introduction: 'A comfort dish my mother would serve me',
    category: 'Italian',
    duration: '30-60 min',
    ingredients: '1lb pasta \n 1lb chicken \n 1-2 cups baby spinach \n 3 cups chicken stock \n' +
        '1 tablespoon olive oil \n kosher salt \n',
    instructions: 'add everything together in a pot, feel free to turn on the heat and cook until done',
    hidden: false,
});


recipe.save((err, user) => {
    if (err) {
        console.log(err);
        return process.exit();
    }
    console.log(`user created: ${user}`);
    return process.exit();
});

recipe = new Recipe({
    title: 'Spinach Chicken Alfredo hidden',
    author: '5a10c5846b75b17dd30ab90e',
    difficulty: 'Medium',
    introduction: 'A comfort dish my mother would serve me',
    category: 'Italian',
    duration: '30-60 min',
    ingredients: '1lb pasta \n 1lb chicken \n 1-2 cups baby spinach \n 3 cups chicken stock \n' +
        '1 tablespoon olive oil \n kosher salt \n',
    instructions: 'add everything together in a pot, feel free to turn on the heat and cook until done',
    hidden: true,
});

