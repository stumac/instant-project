// just a small script to create a user

const User = require('../data_store/User');
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017');
const newUser = new User();

newUser.email = 'stuart.g.macgregor@gmail.com';
newUser.password = 'foobar123';
newUser.name = 'stuart macgregor';

newUser.save((err, user) => {
    if (err) {
        console.log(err);
        return process.exit();
    }
    console.log(`user created: ${user}`);
    return process.exit();
});
